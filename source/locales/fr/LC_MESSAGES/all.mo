��    2      �  C   <      H     I     O     \     k     {     �     �     �     �     �     �     �     �     �     �  -        0     8     =     B  	   O     Y     a     n  	   ~  
   �     �     �     �     �     �  
   �     �     �     �  	                  3     ?     G     M  
   Z     e     l     x          �     �    �  	   �     �     �          2     :     A     J     R     n  	   �     �     �  %   �     �  E   �     2	     9	     I	     N	     _	     u	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	  
   
     
     <
     [
     p
     |
     �
     �
  	   �
     �
     �
     �
     �
     �
          
     '     5         /   &   !   '      $              %                     	                              (   *   ,   2                   -             
                           .   0             1          #      )   +             "                 About Add to Queue Add to library Add to playlist Added Albums Artists Cancel Community Playlists Could not Rename Delete Delete Playlist? Donate Download to Music Folder Enter Name... Find songs to play using the search bar above Go back Loop More More actions Move Down Move Up New Playlist New Playlist... Next song No Results Ok Play Play All Playlist already exists Previous song Radio Mode Remove From Downloads Remove From Queue Rename Playlist Rename... Saved Search for Content... Show Artist Shuffle Songs Toggle pause Top Result Videos View Artist Volume Your Library is Empty Your Playlists translator-credits Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: Irénée Thirion <irenee.thirion@e.email>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.2.2
 À propos Ajouter à la file d’attente Ajouter à la bibliothèque Ajouter à la playlist Ajouté Albums Artistes Annuler Playlists de la communauté Impossible de renommer Supprimer Supprimer la playlist ? Faire un don Télécharger dans le dossier Musique Entrez un nom… Recherchez des titres à jouer depuis la barre de recherche ci-dessus Retour Jouer en boucle Plus Plus d’actions Déplacer vers le bas Déplacer vers le haut Nouvelle playlist Nouvelle playlist… Titre suivant Aucun résultat OK Jouer Jouer tout La playlist existe déjà Titre précédent Mode radio Supprimer des téléchargements Retirer de la file d’attente Renommer la playlist Renommer… Enregistré Rechercher du contenu… Afficher l’artiste Mélanger Titres Mettre en pause Résultat en tête Vidéos Voir l’artiste Volume Votre bibliothèque est vide Vos playlists Irénée Thirion 